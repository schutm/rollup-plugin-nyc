import buble from 'rollup-plugin-buble';

export default {
	input: 'src/index.js',
	plugins: [buble()],
	external: ['istanbul-lib-instrument', 'rollup-pluginutils'],
	output: [
		{
			format: 'cjs',
			file: 'dist/rollup-plugin-nyc.cjs.js'
		},
		{
			format: 'es',
			file: 'dist/rollup-plugin-nyc.es6.js'
		}
	]
};
