TARGET ?= $(CURDIR)
SRCDIR = $(dir $(abspath $(MAKEFILE_LIST)))
OUTDIR = $(dir $(abspath $(TARGET))/)

.SUFFIXES:

ifeq ($(SRCDIR),$(OUTDIR))

TARGET = ../output
MAKETARGET = $(MAKE) --no-print-directory -C $@ -f $(CURDIR)/Makefile $(MAKECMDGOALS)

.PHONY: $(OUTDIR)
$(OUTDIR):
	+@[ -d $@ ] || mkdir -p $@
	+@$(MAKETARGET)

Makefile : ;

% :: $(OUTDIR) ;

.PHONY: clean
clean:
	@rm -rf $(OUTDIR)

.PHONY: lint
lint:
	@yarn run lint
else

INSTALL_PACKAGES := .run-yarn
TEST_FILES = $(wildcard $(SRCDIR)test/*.*) $(wildcard $(SRCDIR)test/**/*.*)
SOURCE_FILES = $(wildcard $(SRCDIR)src/*.*) $(wildcard $(SRCDIR)src/**/*.*)

.PHONY: build
build: dependencies $(OUTDIR)rollup.config.js $(patsubst $(SRCDIR)%,$(OUTDIR)%,$(SOURCE_FILES))
	@yarn build

.PHONY: dependencies
dependencies: $(OUTDIR)package.json $(OUTDIR)yarn.lock
	-@test ! -r $(INSTALL_PACKAGES) || (\
		echo "Installing node packages"; \
		yarn install --prod -s --no-progress   \
	)
	@rm -f $(INSTALL_PACKAGES)

.PHONY: test
test: devDependencies build $(patsubst $(SRCDIR)%,$(OUTDIR)%,$(TEST_FILES))
	@yarn test

.PHONY: devDependencies
devDependencies: $(OUTDIR)package.json $(OUTDIR)yarn.lock
	-@test ! -r $(INSTALL_PACKAGES) || (\
		echo "Installing node packages"; \
		yarn install -s --no-progress   \
	)
	@rm -f $(INSTALL_PACKAGES)

$(OUTDIR)package.json: $(SRCDIR)package.json
	@echo "Copying '$<' to '$@'"
	@cd $(SRCDIR) && find $(<:$(SRCDIR)%=%) | cpio -p -d --quiet $(OUTDIR)
	@touch $(INSTALL_PACKAGES)

$(OUTDIR)yarn.lock: $(SRCDIR)yarn.lock
	@echo "Copying '$<' to '$@'"
	@cd $(SRCDIR) && find $(<:$(SRCDIR)%=%) | cpio -p -d --quiet $(OUTDIR)
	@touch $(INSTALL_PACKAGES)

$(OUTDIR)%: $(SRCDIR)%
	@echo "Copying '$<' to '$@'"
	@cd $(SRCDIR) && find $(<:$(SRCDIR)%=%) | cpio -p -d --quiet $(OUTDIR)

endif
