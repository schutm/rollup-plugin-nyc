project     = ENV.fetch('PROJECT', File.basename(File.dirname(__FILE__)))
base_dir    = File.expand_path(File.dirname(__FILE__) + '/../..')

$HOST = {
	'base_dir'    => ENV.fetch('BASEDIR', base_dir),
	'source_dir'  => ENV.fetch('SOURCEDIR', File.dirname(__FILE__)),
	'output_dir'  => ENV.fetch('OUTPUTDIR', "#{base_dir}/Outputs/#{project}")
}

$GUEST = {
	'base_dir'    => '/project',
	'source_dir'  => "/project/sources"
}

def ensure_dir target
  unless File.directory?(target)
    FileUtils.mkdir_p(target)
  end
end

def provision root
  Dir.glob("#{root}/**/*.provisioner").sort.each do |f|
    puts "Including '#{f}'"
    Vagrant.configure(2) do |config|
      config.vm.provision "shell", path: f
    end
  end
end

ensure_dir($HOST['output_dir'])

Vagrant.configure(2) do |config|
  # Box to work with
  config.vm.box = "debian/contrib-jessie64"

  # Shared folders.
	config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.synced_folder $HOST['source_dir'], $GUEST['source_dir']

  # Make sure we don't get non-interactive shell problems
  config.ssh.shell = "bash"

  # Set owner of shared folder.
  config.vm.provision "shell", inline: "chown vagrant:vagrant #{$GUEST['base_dir']}"
end

provision($HOST['source_dir'])
