import { createFilter } from 'rollup-pluginutils';
import { createInstrumenter } from 'istanbul-lib-instrument';

const defaultOptions = {
	coverageVariable: '__coverage__',
	preserveComments: false,
	compact: true,
	esModules: true,
	autoWrap: true,
	produceSourceMap: true,
	debug: false
};

function cloneObjectWithoutKeys(obj, keysToIgnore) {
	return Object.keys(obj).reduce((result, key) => {
		if (keysToIgnore.indexOf(key) === -1) { // eslint-disable-line no-magic-numbers
			return Object.assign({}, result, { [key]: obj[key] });
		}

		return result;
	}, {});
}

export default function(options = {}) {
	const filter = createFilter(options.include, options.exclude);
	const passedInstrumenterOptions = cloneObjectWithoutKeys(options, ['include', 'exclude']);
	const instrumenterOptions = Object.assign({}, defaultOptions, passedInstrumenterOptions);
	const instrumenter = createInstrumenter(instrumenterOptions);

	return {
		transform(code, id) {
			if (!filter(id)) {
				return null;
			}

			const transformedCode = instrumenter.instrumentSync(code, id);
			const map = instrumenterOptions.produceSourceMap ? instrumenter.lastSourceMap() : { mappings: '' };

			return { code: transformedCode, map };
		}
	};
}
