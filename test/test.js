/* global require, process, __dirname */

import test from 'ava';

const rollup = require('rollup');
const nycPlugin = require('..');

process.chdir(__dirname);

async function generateBundle() {
	const bundle = await rollup.rollup({
		input: 'fixtures/main.js',
		plugins: [nycPlugin()],
		external: ['whatever']
	});

	return bundle.generate({ format: 'cjs', sourcemap: true });
}

test('transforms code through (nyc) istanbul instrumenter', async (t) => {
	const { code } = await generateBundle();

	t.regex(code, /__coverage__/, '__coverage__ not found');
});

test('adds the file name properly', async (t) => {
	const { code } = await generateBundle();

	t.regex(code, /.*fixtures\/main.js.*/, 'fixtures/main.js not found');
});

test('can still run instrumented code', async (t) => {
	const { code } = await generateBundle();
	const equalsOne = eval(code); // eslint-disable-line no-eval

	t.true(equalsOne(1), 'Equals one'); // eslint-disable-line no-magic-numbers
	t.false(equalsOne(0), "Doesn't equals one"); // eslint-disable-line no-magic-numbers
});

test('add sourcemap', async (t) => {
	const { map } = await generateBundle();

	t.truthy(map.mappings, 'Mappings is available in returned structure');
	t.is(map.mappings, ';;;;otBAAA,kCAEA,kBAAO,CAAmBA,GAAnB,CAAwB,2CAC9B,aAAe,CAAf,CACA;;;;', 'Mapping is correct');
});
