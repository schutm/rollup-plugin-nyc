/* eslint no-magic-numbers: off */

export function equalsOne(val) {
	return val === 1;
}
