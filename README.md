# rollup-plugin-nyc

Seamless integration between [Rollup](https://github.com/rollup/rollup) and [NYC](https://github.com/istanbuljs/nyc).


## Why?

If you're using Rollup to generate a standalone bundle you will probably need also to bundle your tests before running
them, and if you want the code coverage report, you will need to instrument the program files before the bundle is
generated to avoid instrumenting also the code of the test files.

That is the reason why rollup-plugin-nyc exists.


## Installation

```bash
npm install --save-dev rollup-plugin-nyc
```


## Usage

```js
import { rollup } from 'rollup';
import nyc from 'rollup-plugin-nyc';

rollup({
  entry: 'main.js',
  plugins: [
    nyc({
      exclude: ['test/**/*.js']
    })
  ]
}).then(...)
```

### Options

All options are optional.

#### `include`

Can be a minimatch pattern or an array of minimatch patterns. If it is omitted or of zero length, files should be
included by default.

#### `exclude`

Can be a minimatch pattern or an array of minimatch patterns. Files to exclude, commonly the test files.

#### other options

All options, except `include` and `exclude` will be passed to NYC / istanbuljs

Default value:

```js
{
	coverageVariable: '__coverage__',
	preserveComments: false,
	compact: true,
	esModules: true,
	autoWrap: true,
	produceSourceMap: true,
	debug: false
}
```

[More info](https://github.com/istanbuljs/nyc/#configuring-nyc) about options.


## License

[MIT](LICENSE.md)
